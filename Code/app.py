from flask import Flask, Response, render_template
from picamera.array import PiRGBArray
from picamera import PiCamera
from threading import Thread
import RPi.GPIO as GPIO
import time
import io
import os
import cv2
import numpy as np
import pickle

app = Flask(__name__)


#GPIO SETTINGS
GPIO.setmode(GPIO.BOARD)

#PIN_ZSTOP OUTPUT Move the TURNTABLE by 1 step (1/1200 of a revolution)
#PIN_ZSTEP OUTPUT Move the camera by 1 step (1/200 of a mm)
#PIN_ZDIR  OUTPUT Sets the direction of travel for the camera
#PIN_ROTSTEP OUTPUT Move the turntable by 1 step (1/100 of a full rotation)
#PIN_ROTDIR OUTPUT Sets the direction of travel for the turntable
#PIN_MOTORDISABLE OUTPUT Disables motors when HIGH
#PIN_LASERENABLE OUTPUT enables lasers when HIGH

class Camera(object):
    thread = None  # background thread that reads frames from camera
    frame = None  # current frame is stored here by background thread
    last_access = 0  # time of last client access to the camera

    def initialize(self):
        if Camera.thread is None:
            # start background frame thread
            Camera.thread = Thread(target=self._thread)
            Camera.thread.start()

            # wait until frames start to be available
            while self.frame is None:
                time.sleep(0)

    def get_frame(self):
        Camera.last_access = time.time()
        self.initialize()
        return self.frame

    @classmethod
    def _thread(cls):
        with PiCamera() as camera:
            # camera setup
            camera.resolution = (3280,2464)
            camera.rotation = 90
            camera.vflip = True
            
            # let camera warm up
            time.sleep(2)

            stream = io.BytesIO()
            for foo in camera.capture_continuous(stream, 'jpeg', use_video_port=True):
                # store frame
                stream.seek(0)
                cls.frame = stream.read()

                # reset stream for next frame
                stream.seek(0)
                stream.truncate()

                # if there hasn't been any clients asking for frames in
                # the last 10 seconds stop the thread
                if time.time() - cls.last_access > 10:
                    break
        cls.thread = None


PIN={"ZSTOP":8,
     "ZSTEP":10,
     "ZDIR":12,
     "ROTSTEP":16,
     "ROTDIR":18,
     "MOTORDISABLE":24,
     "LLASERENABLE":36,
     "RLASERENABLE":38
    }

GPIO.setup(PIN["ZSTEP"], GPIO.OUT)
GPIO.setup(PIN["ZDIR"], GPIO.OUT)
GPIO.setup(PIN["ROTSTEP"], GPIO.OUT)
GPIO.setup(PIN["ROTDIR"], GPIO.OUT)
GPIO.setup(PIN["MOTORDISABLE"], GPIO.OUT)
GPIO.setup(PIN["LLASERENABLE"], GPIO.OUT)
GPIO.setup(PIN["RLASERENABLE"], GPIO.OUT)
GPIO.setup(PIN["ZSTOP"], GPIO.IN, pull_up_down=GPIO.PUD_UP) #Z-STOP is input with a pullup

GPIO.output(PIN["MOTORDISABLE"], GPIO.HIGH) # DISABLE MOTORS
GPIO.output(PIN["LLASERENABLE"], GPIO.LOW) # DISABLE LASERS
GPIO.output(PIN["RLASERENABLE"], GPIO.LOW) # DISABLE LASERS

#DEFAULT SETTINGS
#GANTRY_MOVE_DELAY = Delay between motor pulses moving Z axis
#GANTRY_RAPID_DELAY = Delay between motor pulses rapid moving Z axis
#GANTRY_HEIGHT = Height of Z axis in mm
#GANTRY_STEPSMM  = number of motor steps per mm of gantry movement
#TABLE_STEPSREV = number of motor steps per revolution of turntable
#TABLE_MOVE_DELAY = Delay between motor pulses moving turntable
#USE_LEFT_LASER =  Use the left laser while scanning
#USE_RIGHT_LASER = User the right laser while scanning
#COR = center of rotation
#XSCALE_FACTOR = X axis scaling factor
#YSCALE_FACTOR = Y axis scaling factor
#ZSCALE_FACTOR = Z axis scaling factor

CONFIG_FILE = "scanner.cfg"
config={"GANTRY_MOVE_DELAY":0.01,
         "GANTRY_RAPID_DELAY":0.002,
         "GANTRY_HEIGHT":240,
         "GANTRY_STEPSMM":20,
         "TABLE_STEPSREV":1200,
         "TABLE_MOVE_DELAY":0.01,
         "USE_LEFT_LASER":False,
         "USE_RIGHT_LASER":False,
         "COR":200,
         "XSCALE_FACTOR":1.0,
         "YSCALE_FACTOR":1.0,
         "ZSCALE_FACTOR":1.0
        }

#VARS
BOOT_COMPLETE = True #Has the startup axis homing been done yet?
MOTORS_MOVING = False #Are any motors currently running?
GANTRY_POS = 0 #current height of the gantry in mm from Home

#VIDEO SETTINGS

#video = cv2.VideoCapture(0)
#def gen(video):
#    while True:
#        success, image = video.read()
#        ret, jpeg = cv2.imencode('.jpg', cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE) )
#        frame = jpeg.tobytes()
#        yield (b'--frame\r\n'
#               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        
def getjpg(camera):
    frame = camera.get_frame()
    with open("test.jpg","wb") as f:
        f.write(frame)

    return Response("READY",200)

#WEB INTERFACE 
@app.route('/')
def index():
    if BOOT_COMPLETE:
        #main view page
        return render_template("index.html")
    
    #do the homing
    # RUN THE APP
    p = Thread(target=doBoot, args=())
    p.start()
    return render_template("splash.html")

@app.route('/ready')
def ready():
    # check whether the system is busy. 404 if busy, 200 & READY if ready
    if BOOT_COMPLETE:
        return Response("READY",200)
    return Response("NOT READY",401)
        
@app.route('/video_feed')
def video_feed():
    #video feed from camera
    global rawCapture
    return Response(gen(Camera()), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/settings')
def settings():
    #variables used in context of the settings template
    global context 
    return render_template("settings.html",**config)

# TOP BAR BUTTON HANDLERS
@app.route('/gantryUp')
def gantryUp():
    if GANTRY_POS >= config["GANTRY_HEIGHT"]:
        return Response("Gantry Endstop",401)
    p = Thread(target=gantryMove, args=(True,10))
    p.start()
    return Response("Gantry UP",200)
    
@app.route('/gantryDown')
def gantryDown():
    if GANTRY_POS <= 0:
        return Response("Gantry Endstop",401)

    p = Thread(target=gantryMove, args=(False,10))
    p.start()
    return Response("Gantry UP",200)

    return Response("GantryDown",200)

    
@app.route('/gantryHome')
def gantryHome():
    #Rehome the gantry
    p = Thread(target=zHome, args=())
    p.start()
    return render_template("splash.html")

@app.route('/setZMax')
def setZMax():
    #height to end scan
    pass

@app.route('/setZMin')
def setZMin():
    #height to start scan
    pass

# BOTTOM BAR BUTTON HANDLERS
@app.route('/calibrate')
def doCalib():
    #calibrate the scanner
    pass

@app.route('/doScan')
def doScan():
    getjpg(Camera())

    #TODO
    #start the scanning process
    #p = Thread(target=doTest, args=())
    #p.start()
    return Response("TESTING",200)







def tableMove (ccw,steps):
    # Move the turntable by steps
    global MOTORS_MOVING
    
    MOTORS_MOVING = True
    GPIO.output(PIN["ROTDIR"],ccw)
    for i in range(steps):
        GPIO.output(PIN["ROTSTEP"],GPIO.HIGH)
        time.sleep(config["TABLE_MOVE_DELAY"])    
        GPIO.output(PIN["ROTSTEP"],GPIO.LOW)
        time.sleep(config["TABLE_MOVE_DELAY"])    
    MOTORS_MOVING = False

def loadConfig():
    #loads the config file and updates the variables
    global config
    try:
        f = open(CONFIG_FILE, 'rb')
        config = pickle.load(f)
        f.close()
    except EOFError:
        print("Config File not found. Creating Default File.")
        saveConfig()
    
    
def saveConfig():
    #saves the config file and updates the variables
    f = open(CONFIG_FILE, 'wb') 
    pickle.dump(config, f)          # dump data to file
    f.close()    
    
def doBoot():
    # load up the config files and home the axes
    global BOOT_COMPLETE
    BOOT_COMPLETE = False
    loadConfig()
    zHome()
    BOOT_COMPLETE = True
    
def doTest():
    GPIO.output(PIN["LLASERENABLE"], GPIO.HIGH) 
    GPIO.output(PIN["RLASERENABLE"], GPIO.LOW) 
    tableMove(True,200)
    GPIO.output(PIN["LLASERENABLE"], GPIO.LOW) 
    GPIO.output(PIN["RLASERENABLE"], GPIO.HIGH) 
    tableMove(False,200)
    GPIO.output(PIN["LLASERENABLE"], GPIO.LOW)
    GPIO.output(PIN["RLASERENABLE"], GPIO.LOW) 
    gantryMove(True,20)
    GPIO.output(PIN["LLASERENABLE"], GPIO.HIGH)
    GPIO.output(PIN["RLASERENABLE"], GPIO.LOW)
    tableMove(True,200)
    GPIO.output(PIN["LLASERENABLE"], GPIO.LOW) 
    GPIO.output(PIN["RLASERENABLE"], GPIO.HIGH)
    tableMove(False,200)
    GPIO.output(PIN["LLASERENABLE"], GPIO.LOW) 
    GPIO.output(PIN["RLASERENABLE"], GPIO.LOW) 

def gantryMove(up, mm):
    # Move the gantry by mm
    # up = true moves motor up, otherwise down
    global MOTORS_MOVING
    global GANTRY_POS
    
    MOTORS_MOVING = True
    GPIO.output(PIN["ZDIR"],up)
    for i in range(config["GANTRY_STEPSMM"]*mm):
        if GPIO.input(PIN["ZSTOP"]):
            GPIO.output(PIN["ZSTEP"],GPIO.HIGH)
            time.sleep(config["GANTRY_RAPID_DELAY"])
            GPIO.output(PIN["ZSTEP"],GPIO.LOW)
            time.sleep(config["GANTRY_RAPID_DELAY"])    
    MOTORS_MOVING = False
    if up:
        GANTRY_POS+=mm
    else:
        GANTRY_POS-=mm
        
def zHome():
    #home the z-axis
    global MOTORS_MOVING
    global GANTRY_POS

    MOTORS_MOVING = True
    GPIO.output(PIN["MOTORDISABLE"],GPIO.LOW) # Enable the motors
    
    #wind the carriage up until it hits the endstop
    GPIO.output(PIN["ZDIR"],GPIO.HIGH) # Carriage direction is up
    while GPIO.input(PIN["ZSTOP"]):
        GPIO.output(PIN["ZSTEP"],GPIO.HIGH)
        time.sleep(config["GANTRY_RAPID_DELAY"])
        GPIO.output(PIN["ZSTEP"],GPIO.LOW)
        time.sleep(config["GANTRY_RAPID_DELAY"])
        
    # wind the carriage back down again
    GPIO.output(PIN["ZDIR"],GPIO.LOW) # Carriage direction is down
    for i in range(config["GANTRY_HEIGHT"]*config["GANTRY_STEPSMM"]):
        GPIO.output(PIN["ZSTEP"],GPIO.HIGH)
        time.sleep(config["GANTRY_RAPID_DELAY"])
        GPIO.output(PIN["ZSTEP"],GPIO.LOW)
        time.sleep(config["GANTRY_RAPID_DELAY"])
    
    # mark the boot process complete
    MOTORS_MOVING = False
    GANTRY_POS = 0

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=2204, threaded=True)   