EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2150 1650 2300 1650
Wire Wire Line
	2300 1650 2300 950 
Wire Wire Line
	2300 950  3300 950 
Wire Wire Line
	3300 950  3300 1200
Wire Wire Line
	2150 1750 2400 1750
Wire Wire Line
	2400 1750 2400 1000
Wire Wire Line
	2400 1000 3100 1000
Wire Wire Line
	3100 1000 3100 1200
$Comp
L Motor:Stepper_Motor_bipolar M_CAM1
U 1 1 5FAEA47A
P 3200 1500
F 0 "M_CAM1" H 3388 1624 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 3388 1533 50  0000 L CNN
F 2 "" H 3210 1490 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 3210 1490 50  0001 C CNN
	1    3200 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1400 2500 1400
Wire Wire Line
	2500 1400 2500 1850
Wire Wire Line
	2500 1850 2150 1850
Wire Wire Line
	2900 1600 2600 1600
Wire Wire Line
	2600 1600 2600 1950
Wire Wire Line
	2600 1950 2150 1950
Wire Wire Line
	1250 1350 1150 1350
Wire Wire Line
	1150 1350 1150 1450
Wire Wire Line
	1150 1450 1250 1450
NoConn ~ 1250 2250
NoConn ~ 1250 2150
NoConn ~ 1250 2050
Text GLabel 1250 1850 0    50   Input ~ 0
Zdir
Text GLabel 1250 1750 0    50   Input ~ 0
zstep
Text GLabel 1250 1650 0    50   Input ~ 0
Menable
$Comp
L Transistor_BJT:2N2219 QL1
U 1 1 5FAEB9EC
P 9900 1700
F 0 "QL1" H 10090 1746 50  0000 L CNN
F 1 "2N2222" H 10090 1655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 10100 1625 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 9900 1700 50  0001 L CNN
	1    9900 1700
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A2
U 1 1 5FB11359
P 1650 4100
F 0 "A2" H 1700 4981 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 1700 4890 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 1925 3350 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 1750 3800 50  0001 C CNN
	1    1650 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4000 2300 4000
Wire Wire Line
	2300 4000 2300 3300
Wire Wire Line
	2300 3300 3300 3300
Wire Wire Line
	3300 3300 3300 3550
Wire Wire Line
	2150 4100 2400 4100
Wire Wire Line
	2400 4100 2400 3350
Wire Wire Line
	2400 3350 3100 3350
Wire Wire Line
	3100 3350 3100 3550
$Comp
L Motor:Stepper_Motor_bipolar M_TTAB1
U 1 1 5FB11367
P 3200 3850
F 0 "M_TTAB1" H 3388 3974 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 3388 3883 50  0000 L CNN
F 2 "" H 3210 3840 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 3210 3840 50  0001 C CNN
	1    3200 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3750 2500 3750
Wire Wire Line
	2500 3750 2500 4200
Wire Wire Line
	2500 4200 2150 4200
Wire Wire Line
	2900 3950 2600 3950
Wire Wire Line
	2600 3950 2600 4300
Wire Wire Line
	2600 4300 2150 4300
Wire Wire Line
	1250 3700 1150 3700
Wire Wire Line
	1150 3700 1150 3800
Wire Wire Line
	1150 3800 1250 3800
NoConn ~ 1250 4600
NoConn ~ 1250 4500
NoConn ~ 1250 4400
Text GLabel 1250 4200 0    50   Input ~ 0
Rdir
Text GLabel 1250 4100 0    50   Input ~ 0
rstep
Text GLabel 1250 4000 0    50   Input ~ 0
Menable
$Comp
L Transistor_BJT:2N2219 QR1
U 1 1 5FB11388
P 9900 2950
F 0 "QR1" H 10090 2996 50  0000 L CNN
F 1 "2N2222" H 10090 2905 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 10100 2875 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 9900 2950 50  0001 L CNN
	1    9900 2950
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A1
U 1 1 5FAE7D5D
P 1650 1750
F 0 "A1" H 1700 2631 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 1700 2540 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 1925 1000 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 1750 1450 50  0001 C CNN
	1    1650 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5FB1C38D
P 1650 2550
F 0 "#PWR0101" H 1650 2300 50  0001 C CNN
F 1 "GND" H 1655 2377 50  0000 C CNN
F 2 "" H 1650 2550 50  0001 C CNN
F 3 "" H 1650 2550 50  0001 C CNN
	1    1650 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FB1D727
P 1850 2550
F 0 "#PWR0102" H 1850 2300 50  0001 C CNN
F 1 "GND" H 1855 2377 50  0000 C CNN
F 2 "" H 1850 2550 50  0001 C CNN
F 3 "" H 1850 2550 50  0001 C CNN
	1    1850 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5FB1DFCF
P 1650 4900
F 0 "#PWR0103" H 1650 4650 50  0001 C CNN
F 1 "GND" H 1655 4727 50  0000 C CNN
F 2 "" H 1650 4900 50  0001 C CNN
F 3 "" H 1650 4900 50  0001 C CNN
	1    1650 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5FB1E55D
P 1850 4900
F 0 "#PWR0104" H 1850 4650 50  0001 C CNN
F 1 "GND" H 1855 4727 50  0000 C CNN
F 2 "" H 1850 4900 50  0001 C CNN
F 3 "" H 1850 4900 50  0001 C CNN
	1    1850 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5FB1E9BA
P 10000 3150
F 0 "#PWR0105" H 10000 2900 50  0001 C CNN
F 1 "GND" H 10005 2977 50  0000 C CNN
F 2 "" H 10000 3150 50  0001 C CNN
F 3 "" H 10000 3150 50  0001 C CNN
	1    10000 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5FB1F1D2
P 10000 1900
F 0 "#PWR0106" H 10000 1650 50  0001 C CNN
F 1 "GND" H 10005 1727 50  0000 C CNN
F 2 "" H 10000 1900 50  0001 C CNN
F 3 "" H 10000 1900 50  0001 C CNN
	1    10000 1900
	1    0    0    -1  
$EndComp
Text GLabel 9700 1700 0    50   Input ~ 0
LLASER
Text GLabel 9700 2950 0    50   Input ~ 0
RLASER
$Comp
L power:+12V #PWR0114
U 1 1 5FB399C6
P 1850 3400
F 0 "#PWR0114" H 1850 3250 50  0001 C CNN
F 1 "+12V" V 1865 3528 50  0000 L CNN
F 2 "" H 1850 3400 50  0001 C CNN
F 3 "" H 1850 3400 50  0001 C CNN
	1    1850 3400
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR0115
U 1 1 5FB3C29D
P 1850 1050
F 0 "#PWR0115" H 1850 900 50  0001 C CNN
F 1 "+12V" V 1865 1178 50  0000 L CNN
F 2 "" H 1850 1050 50  0001 C CNN
F 3 "" H 1850 1050 50  0001 C CNN
	1    1850 1050
	0    1    1    0   
$EndComp
$Comp
L Device:Laserdiode_1C2A LD1
U 1 1 5FB4D7D8
P 10000 1200
F 0 "LD1" V 9996 1112 50  0000 R CNN
F 1 "Laserdiode_1C2A" V 9905 1112 50  0000 R CNN
F 2 "" H 9900 1175 50  0001 C CNN
F 3 "~" H 10030 1000 50  0001 C CNN
	1    10000 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:Laserdiode_1C2A LD2
U 1 1 5FB51DBB
P 10000 2450
F 0 "LD2" V 9996 2362 50  0000 R CNN
F 1 "Laserdiode_1C2A" V 9905 2362 50  0000 R CNN
F 2 "" H 9900 2425 50  0001 C CNN
F 3 "~" H 10030 2250 50  0001 C CNN
	1    10000 2450
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5FB5F202
P 10000 1000
F 0 "#PWR?" H 10000 850 50  0001 C CNN
F 1 "+5V" V 10015 1128 50  0000 L CNN
F 2 "" H 10000 1000 50  0001 C CNN
F 3 "" H 10000 1000 50  0001 C CNN
	1    10000 1000
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5FB5FCFD
P 10000 2250
F 0 "#PWR?" H 10000 2100 50  0001 C CNN
F 1 "+5V" V 10015 2378 50  0000 L CNN
F 2 "" H 10000 2250 50  0001 C CNN
F 3 "" H 10000 2250 50  0001 C CNN
	1    10000 2250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Raspberry_Pi_2_3 J?
U 1 1 5FB8948F
P 7000 2550
F 0 "J?" H 7400 3950 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 7700 3850 50  0000 C CNN
F 2 "" H 7000 2550 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 7000 2550 50  0001 C CNN
	1    7000 2550
	1    0    0    -1  
$EndComp
Text GLabel 6200 2450 0    50   Input ~ 0
RLASER
Text GLabel 6200 1950 0    50   Input ~ 0
LLASER
$Comp
L Switch:SW_SPDT SWZ1
U 1 1 5FB1AA8E
P 5250 1550
F 0 "SWZ1" H 5250 1835 50  0000 C CNN
F 1 "SW_SPDT" H 5250 1744 50  0000 C CNN
F 2 "" H 5250 1550 50  0001 C CNN
F 3 "~" H 5250 1550 50  0001 C CNN
	1    5250 1550
	1    0    0    -1  
$EndComp
Text GLabel 6200 2950 0    50   Input ~ 0
Rdir
Text GLabel 6200 2850 0    50   Input ~ 0
rstep
Text GLabel 6200 2150 0    50   Input ~ 0
Zdir
Text GLabel 6200 1750 0    50   Input ~ 0
zstep
Text GLabel 7800 2750 2    50   Input ~ 0
Menable
Text GLabel 1650 1050 0    50   Input ~ 0
VLogic
Text GLabel 1650 3400 0    50   Input ~ 0
VLogic
Text GLabel 7100 1250 1    50   Input ~ 0
VLogic
$Comp
L power:+5V #PWR?
U 1 1 5FBA13D0
P 6800 1250
F 0 "#PWR?" H 6800 1100 50  0001 C CNN
F 1 "+5V" V 6815 1378 50  0000 L CNN
F 2 "" H 6800 1250 50  0001 C CNN
F 3 "" H 6800 1250 50  0001 C CNN
	1    6800 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5450 1650 6200 1650
Wire Wire Line
	5050 1550 5050 3850
Wire Wire Line
	5050 3850 6600 3850
$Comp
L power:GND #PWR?
U 1 1 5FBA7106
P 6600 3850
F 0 "#PWR?" H 6600 3600 50  0001 C CNN
F 1 "GND" H 6605 3677 50  0000 C CNN
F 2 "" H 6600 3850 50  0001 C CNN
F 3 "" H 6600 3850 50  0001 C CNN
	1    6600 3850
	1    0    0    -1  
$EndComp
Connection ~ 6600 3850
Text Notes 7350 7500 0    59   ~ 12
3D Scanner Connections
Text Notes 8200 7650 0    59   ~ 12
13-NOV-2020
Text Notes 10600 7650 0    59   ~ 12
1
Text Notes 7000 6850 0    59   ~ 12
Raspberry Pi connections for 3D scanner. \n+12v into scanner goes to motor power on A4988, stepped down to 5v for Raspberry Pi.\n3.3v from Pi powers logic side of A4988.
$EndSCHEMATC
