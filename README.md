**MidgeScan 3D Scanner**

MidgeScan is a Raspberry Pi powered desktop 3D scanner, featuring a touchscreen and wen interface, dual lasers, stepper motor controlled turntable and Z-axis gantry. The interface is based around Python and Flask. STL files for 3D printing, parts list, and build instructions will all appear in this repository. The Scanner is currently in very early Alpha. Most notably, it DOES NOT YET HAVE THE ABILITY TO SCAN THINGS.

Things that work: 
Touchscreen
Motor control
Z axis homing
Laser control

Things being worked on:
Touchscreen UI and web UI

Things that don't work yet:
3D scanning